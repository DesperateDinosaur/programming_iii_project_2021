import java.util.ArrayList;
import java.util.List;

public class Proportional extends Poll{
    /**
     * Takes a Vote List, Party List, Riding List, and Candidaet List as input 
     * and invokes the super constructor
     * @param votes
     * @param parties
     * @param ridings
     * @param candidates
     */
    public Proportional(List<Vote> votes, List<Party> parties, List<Riding> ridings, List<Candidate> candidates){
        super(votes, parties, ridings, candidates);
    }

    /**
     * Loops through ridings and votes to count how many votes belong to each riding
     * For each riding, loops through votes and candidates and counts how many votes each candidate has
     * Sets the leader of that riding to the candidate who had the most votes
     * Loops through ridings and each party, counts how many ridings have a leader who belongs to each party, updates numSeats in each party
     */
    public void countVotes(){
        /*Loop through Riding List and create a unique Vote List for each riding
          Loop through Vote List and add a vote to the ridingVotes List for each riding*/
        for(int i = 0; i < this.ridings.size(); i++){
            List<Vote> ridingVotes = new ArrayList<>();
            for(int j = 0; j < this.votes.size(); j++){
                if(this.votes.get(j).getRiding().equals(this.ridings.get(i).getRidingName())){
                    ridingVotes.add(votes.get(j));
                }
            }
            
            //Variable declaration
            String winner = "";
            int totalVotes = 0;
            
            //Loop through ridingVotes, create a String to temporarily store a candidate and create an int to temporarily store how often they show up
            for(int j = 0; j < ridingVotes.size(); j++){
                String tempCandidate = ridingVotes.get(j).getCandidate();
                int occurances = 0;
                for(int k = 0; k < ridingVotes.size(); k++){
                    /*Loop through candidats in ridingVotes and see how often they appear
                    Increment occurances, set candidate to tempCandidate, and set totalVotes to occurances*/
                    if(ridingVotes.get(k).getCandidate().equals(tempCandidate)){
                        occurances++;
                        if(occurances > totalVotes){
                            winner = tempCandidate;
                            totalVotes = occurances;
                        }
                    }
                }
            }
            //Set the Leader of the Riding to be the winner
            this.ridings.get(i).setLeader(winner);

            //Loop through Candidate List and set the amount of seats each Party has based off how many of their Candidates won a Diding
            for(int j = 0; j < candidates.size(); j++){
                if(candidates.get(j).getName().equals(winner)){
                    candidates.get(j).getParty().setSeats(candidates.get(j).getParty().getSeats() + 1);
                }
            }
        }
    }

    /**
     * Returns String describing how many seats each party has
     * @return
     */
    public String toString(){
        String output = "";
        output += "\n" + "Proportional election chosen" + "\n" + "\n";
        output += "Number of votes in election: " + votes.size() + "\n";
        output += "Number of parties in election: " + parties.size() + "\n";
        output += "Number of ridings in election: " + ridings.size() + "\n";
        output += "Number of candidates in election: " + candidates.size() + "\n" + "\n";
        output += "RESULTS:" + "\n" + "\n";

        for(int i = 0; i < this.parties.size(); i++){
            output += "The " + parties.get(i).getPartyName() + " party owns " + parties.get(i).getSeats() + " seat";
            if(parties.get(i).getSeats() > 1 || parties.get(i).getSeats() == 0){
                output += "s";
            }
            output += "\n";
        }
        return output;
    }
}