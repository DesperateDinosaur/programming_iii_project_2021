import java.util.List;

public class First_Past extends Poll{
    //fields
    private Candidate winner;
    private int post;
    private boolean isPastPost;

    /**
     * This constructor takes a Vote List, Party List, Riding List, and Candidate List as input and invokes it's super constructor
     * It then initializes the post field by calculating 50% of votes and initializes the isPastPost field with false
     * @param votes
     * @param parties
     * @param ridings
     * @param candidates
     */
    public First_Past(List<Vote> votes, List<Party> parties, List<Riding> ridings, List<Candidate> candidates){
        super(votes, parties, ridings, candidates);
        this.post = votes.size()/2; //assuming we will only ever want a 50% first past the post
        this.isPastPost = false;
    }

    /**
     * Loops through votes and candidates and performs checks to determine which candidate passes the post first
     * Sets the winner field to whoever passes the post first
     */
    public void countVotes(){
        final int FIRST_CANDIDATE = 0;
        for(int i = 0; i < this.votes.size(); i++){
            for(int j=0; j<this.candidates.size(); j++){
                if(this.votes.get(i).getCandidate().equals(this.candidates.get(j).getName())){
                    this.candidates.get(j).addVote();
                    if(this.candidates.get(j).getNumVotes() >= this.post){
                        this.winner = this.candidates.get(j);
                        this.isPastPost = true;
                    }
                    else{
                        this.winner = this.candidates.get(FIRST_CANDIDATE);
                        for(int k=0; k<this.candidates.size(); k++){
                            if(candidates.get(k).getNumVotes() > this.winner.getNumVotes()){
                                this.winner = candidates.get(k);
                            }
                        }
                    }
                }
            }
        }
    }
    
    /**
     * Returns winner
     * 
     * @return
     */
    public Candidate getWinner(){
        return this.winner;
    }

    /**
     * If a candidate passes the post, returns string describing winning candidate
     * If no candidate passes the post, treats vote like majority, returns string describing winning candidate by votes
     * @return String
     */
    public String toString(){
        String output = "";
        output += "\n" + "First past the post election chosen" + "\n" + "\n";
        output += "Number of votes in election: " + votes.size() + "\n";
        output += "Number of parties in election: " + parties.size() + "\n";
        output += "Number of ridings in election: " + ridings.size() + "\n";
        output += "Number of candidates in election: " + candidates.size() + "\n" + "\n";
        output += "RESULTS:" + "\n" + "\n";
        if(isPastPost){
            output += this.winner + "\n" + "Has past the post" + "\n";
        }
        else{
            output += this.winner + "\n" + "Has the most votes" + "\n";
        }
        return output;
    }
}