import java.util.List;

public class Majority extends Poll{
    //fields
    private Party winner;
    private int totalVotes;

    /**
     * This constructor takes a Vote List, Party List, Riding List, and Candidate List as input and invokes it's super constructor
     * It then initializes the totalVotes field with the size of the Vote List
     * 
     * @param votes
     * @param parties
     * @param ridings
     * @param candidates
     */
    public Majority(List<Vote> votes, List<Party> parties, List<Riding> ridings, List<Candidate> candidates){
        super(votes, parties, ridings, candidates);
        this.totalVotes = votes.size();
    }

    /**
     * Loops through the votes and parties and counts how many votes each party has
     * Update each party object's numVotes field to how many votes they have
     * Compares these numVotes fields and sets the winner field to the winning party
     */
    public void countVotes(){
        //Integer to fetch party without using a "magic number"
        final int FIRST_PARTY = 0;

        /*Loop through the Vote List and Party List and count how many votes each Party has
          Add each vote that a party receives to their total number of votes*/
        for(int i = 0; i < totalVotes; i++){
            for(int j=0; j<parties.size(); j++){
                if(votes.get(i).getParty().equals(parties.get(j).getPartyName())){
                    parties.get(j).addVote();
                }
            }
        }

        /*Loop through the Party List and check how many votes each party has
          Setting the winner field to the Party object that has the most votes*/
        for(int i=0; i<parties.size(); i++){
            this.winner = parties.get(FIRST_PARTY);
            if(parties.get(i).getNumVotes() > this.winner.getNumVotes()){
                this.winner = parties.get(i);
            }
        }
    }

    /**
     * Returns winner
     * @return
     */
    public Party getWinner(){
        return this.winner;
    }

    /**
     * Returns string describing winning candidate and their vote count
     * @return
     */
    public String toString(){
        String output = "";
        output += "\n" + "Majority election chosen" + "\n" + "\n";
        output += "Number of votes in election: " + votes.size() + "\n";
        output += "Number of parties in election: " + parties.size() + "\n";
        output += "Number of ridings in election: " + ridings.size() + "\n";
        output += "Number of candidates in election: " + candidates.size() + "\n" + "\n";
        output += "RESULTS:" + "\n" + "\n";
        output += "The " + this.winner.getPartyName() + " party wins with " +this.winner.getNumVotes() + " votes" + "\n";
        return output;
    }
}