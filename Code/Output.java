import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.charset.*;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.text.Text;

public class Output extends FileIO{
    //fields
    private Path path;
    private List<String> output;

    /**
     * This constructor takes a filepath String as input and invokes it's super constructor
     * It then sets the path field and initializes the arraylist
     * 
     * @param filepath
     */
    public Output(String filepath){
        super(filepath);
        this.path = Paths.get(this.filepath);
        this.output = new ArrayList<String>();
    }

    /** 
     * Build output StringList using ArrayList
     * This output is created using the results from the GUI
     * 
     * @param Text
     */
    public void stringBuilder(Text resultText){
        this.output.add(resultText.getText());
        this.output.add("------------------------------------");
    }

    /**
     * Call stringBuilder() to build output StringList
     * Check if file at specified path already exists
     * If it does not already exist, create the file and write each element of the output List in the file as a separate line
     * If it does already exist, append each element of the output List in the file as a separate line to the existing file
     */
    public void createFile(Text resultText){
        try {
            //Build StringList for output to the fild
            stringBuilder(resultText);

            //Create new file and check if it exists
            File file = new File(this.filepath);

            //File does not exists: create new file and write output it
            if(file.createNewFile()){
                Files.write(path, this.output, StandardCharsets.UTF_8);
            }
            //File exists: append output to existing file
            else{
                Files.write(path, this.output, StandardOpenOption.APPEND);
            }
        }
        catch (IOException e){
            System.out.println("An error occured: " + e);
        }
    }
}