import java.io.IOException;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;

public class Input extends FileIO{
    //fields
    private boolean fileReadSuccess;

    /**
     * This constructor takes a filepath String as input and invokes it's super constructor
     * @param filepath
     */
    public Input(String filepath){
        super(filepath);
    }

    /**
     * Reads the file's contents and checks if each line of text is formatted by our pre-defined standard
     * Parses through each line and builds a Vote object List, ignoring any lines that don't pass our check
     * @return List<Vote>
     * @throws IOException
     */
    public List<Vote> getVotes() throws IOException{
        //Generate blank ArrayList of Vote objects
        List<Vote> votes = new ArrayList<>();

        //Get filepath of the input file and read all lines
        Path p = Paths.get(getFilepath());
        List<String> lines = Files.readAllLines(p);

        /*Parse through each line of the input file and check if it is valid based off of our predefined format
          If the current line passes the check, create a Vote object with that line's information and add the object to the Vote List
          If the current line fails the check, ignore it*/
        for(String line:lines){
            String[] pieces = line.split(",");
            final int EXPECTED = 3;

            if(pieces.length == EXPECTED){
                String riding = pieces[2];
                String party = pieces[1];
                String candidate = pieces[0];
                Vote v = new Vote(candidate, party, riding);
                votes.add(v);                
            }
        }
        if(votes.size() > 1){
            this.fileReadSuccess = true;
        }
        return votes;
    }

    public boolean getFileReadSuccess(){
        return this.fileReadSuccess;
    }
}