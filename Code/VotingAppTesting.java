import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;

public class VotingAppTesting {
    /**
      IMPORTANT!!
      
      Switch the filepath for the Input in each test case with the absolute filepath that points to the appropriate file on your PC
      The test cases are organized by file in this order: Sample_1.txt, Sample_2.txt, Sample_3.txt
      These files are all located in the included VoteInput directory
     */




    /**
     * Sample_1.txt test cases
     */


    /**
     * Test number of votes in Sample_1.txt
     * 
     * @throws IOException
     */
    @Test
    public void testSampleOneVotes() throws IOException{
        int expected = 146;

        //Use appropriate path here
        Input input = new Input("G:/CompSci_Sem_3/Programming_III/Project/programming_iii_project_2021/VoteInput/Sample_1.txt");
        List<Vote> votes = input.getVotes();

        assertEquals(expected, votes.size());
    }
    
    /**
     * Test number of parties in Sample_1.txt
     * 
     * @throws IOException
     */
    @Test
    public void testSampleOneParties() throws IOException{
        int expected = 5; //Andrew sets his own filepath

        //Use appropriate path here
        Input input = new Input("G:/CompSci_Sem_3/Programming_III/Project/programming_iii_project_2021/VoteInput/Sample_1.txt");
        List<Vote> votes = input.getVotes();
        logic logic = new logic(votes);

        List<Party> parties = logic.createParties();
        assertEquals(expected, parties.size());
    }
    
    /**
     * Test number of ridings in Sample_1.txt
     * 
     * @throws IOException
     */
    @Test
    public void testSampleOneRidings() throws IOException{
        int expected = 5; //Andrew sets his own filepath
        
        //Use appropriate path here
        Input input = new Input("G:/CompSci_Sem_3/Programming_III/Project/programming_iii_project_2021/VoteInput/Sample_1.txt");
        List<Vote> votes = input.getVotes();
        logic logic = new logic(votes);

        List<Riding> ridings = logic.createRidings();
        assertEquals(expected, ridings.size());
    }
    
    /**
     * Test number of candidates in Sample_1.txt
     * 
     * @throws IOException
     */
    @Test
    public void testSampleOneCandidates() throws IOException{
        int expected = 25; //Andrew set his own filepath
        
        //Use appropriate path here
        Input input = new Input("G:/CompSci_Sem_3/Programming_III/Project/programming_iii_project_2021/VoteInput/Sample_1.txt");
        List<Vote> votes = input.getVotes();
        logic logic = new logic(votes);

        List<Party> parties = logic.createParties();
        List<Riding> ridings = logic.createRidings();
        List<Candidate> candidates = logic.createCandidates(parties, ridings);

        assertEquals(expected, candidates.size());
    }
    
    /**
     * Test winner of majority vote in Sample_1.txt
     * 
     * @throws IOException
     */
    @Test
    public void testSampleOneMajority() throws IOException{
        String expected = "Plagiarism"; //Andrew set his own filepath
        
        //Use appropriate path here
        Input input = new Input("G:/CompSci_Sem_3/Programming_III/Project/programming_iii_project_2021/VoteInput/Sample_1.txt");
        List<Vote> votes = input.getVotes();
        logic logic = new logic(votes);

        List<Party> parties = logic.createParties();
        List<Riding> ridings = logic.createRidings();
        List<Candidate> candidates = logic.createCandidates(parties, ridings);

        Majority majority = new Majority(votes, parties, ridings, candidates);
        majority.countVotes();

        assertEquals(expected, majority.getWinner().getPartyName());
    }
    
    /**
     * Test winner of proportional vote in Sample_1.txt
     * 
     * @throws IOException
     */
    @Test
    public void testSampleOneProportional() throws IOException{
        ArrayList<Integer> expected = new ArrayList<Integer>();
        for (int i = 0; i < 5; i++){
            expected.add(1);
        }
        
        //Use appropriate path here
        Input input = new Input("G:/CompSci_Sem_3/Programming_III/Project/programming_iii_project_2021/VoteInput/Sample_1.txt");
        List<Vote> votes = input.getVotes();
        logic logic = new logic(votes);

        List<Party> parties = logic.createParties();
        List<Riding> ridings = logic.createRidings();
        List<Candidate> candidates = logic.createCandidates(parties, ridings);

        Proportional proportional = new Proportional(votes, parties, ridings, candidates);
        proportional.countVotes();

        ArrayList<Integer> results = new ArrayList<Integer>();
        for (int i = 0; i < parties.size(); i++){
            results.add(parties.get(i).getSeats());
        }

        assertEquals(expected, results);
    }
    
    /**
     * Test winner of first past the post vote in Sample_1.txt
     * 
     * @throws IOException
     */
    @Test
    public void testSampleOneFirstPast() throws IOException{
        String expected = "Andrew Bodzay";
        
        //Use appropriate path here
        Input input = new Input("G:/CompSci_Sem_3/Programming_III/Project/programming_iii_project_2021/VoteInput/Sample_1.txt");
        List<Vote> votes = input.getVotes();
        logic logic = new logic(votes);

        List<Party> parties = logic.createParties();
        List<Riding> ridings = logic.createRidings();
        List<Candidate> candidates = logic.createCandidates(parties, ridings);

        First_Past firstPast = new First_Past(votes, parties, ridings, candidates);
        firstPast.countVotes();

        assertEquals(expected, firstPast.getWinner().getName());
    }




    /**
     * Sample_2.txt test cases
     */


    /**
     * Test number of votes in Sample_2.txt
     * 
     * @throws IOException
     */
    @Test
    public void testSampleTwoVotes() throws IOException{
        int expected = 350;
        
        //Use appropriate path here
        Input input = new Input("G:/CompSci_Sem_3/Programming_III/Project/programming_iii_project_2021/VoteInput/Sample_2.txt");
        List<Vote> votes = input.getVotes();

        assertEquals(expected, votes.size());
    }
    
    /**
     * Test number of parties in Sample_2.txt
     * 
     * @throws IOException
     */
    @Test
    public void testSampleTwoParties() throws IOException{
        int expected = 5; //Andrew sets his own filepath
        
        //Use appropriate path here
        Input input = new Input("G:/CompSci_Sem_3/Programming_III/Project/programming_iii_project_2021/VoteInput/Sample_2.txt");
        List<Vote> votes = input.getVotes();
        logic logic = new logic(votes);

        List<Party> parties = logic.createParties();
        assertEquals(expected, parties.size());
    }
    
    /**
     * Test number of ridings in Sample_2.txt
     * 
     * @throws IOException
     */
    @Test
    public void testSampleTwoRidings() throws IOException{
        int expected = 5; //Andrew sets his own filepath
        
        //Use appropriate path here
        Input input = new Input("G:/CompSci_Sem_3/Programming_III/Project/programming_iii_project_2021/VoteInput/Sample_2.txt");
        List<Vote> votes = input.getVotes();
        logic logic = new logic(votes);

        List<Riding> ridings = logic.createRidings();
        assertEquals(expected, ridings.size());
    }
    
    /**
     * Test number of candidates in Sample_2.txt
     * 
     * @throws IOException
     */
    @Test
    public void testSampleTwoCandidates() throws IOException{
        int expected = 25; //Andrew set his own filepath
        
        //Use appropriate path here
        Input input = new Input("G:/CompSci_Sem_3/Programming_III/Project/programming_iii_project_2021/VoteInput/Sample_2.txt");
        List<Vote> votes = input.getVotes();
        logic logic = new logic(votes);

        List<Party> parties = logic.createParties();
        List<Riding> ridings = logic.createRidings();
        List<Candidate> candidates = logic.createCandidates(parties, ridings);

        assertEquals(expected, candidates.size());
    }
    
    /**
     * Test winner of majority vote in Sample_2.txt
     * 
     * @throws IOException
     */
    @Test
    public void testSampleTwoMajority() throws IOException{
        String expected = "MissIndependent"; //Andrew set his own filepath
        
        //Use appropriate path here
        Input input = new Input("G:/CompSci_Sem_3/Programming_III/Project/programming_iii_project_2021/VoteInput/Sample_2.txt");
        List<Vote> votes = input.getVotes();
        logic logic = new logic(votes);

        List<Party> parties = logic.createParties();
        List<Riding> ridings = logic.createRidings();
        List<Candidate> candidates = logic.createCandidates(parties, ridings);

        Majority majority = new Majority(votes, parties, ridings, candidates);
        majority.countVotes();

        assertEquals(expected, majority.getWinner().getPartyName());
    }
    
    /**
     * Test winner of proportional vote in Sample_2.txt
     * 
     * @throws IOException
     */
    @Test
    public void testSampleTwoProportional() throws IOException{
        ArrayList<Integer> expected = new ArrayList<Integer>();
        
        expected.add(1);
        expected.add(2);
        expected.add(2);
        expected.add(0);
        expected.add(0);
        
        //Use appropriate path here
        Input input = new Input("G:/CompSci_Sem_3/Programming_III/Project/programming_iii_project_2021/VoteInput/Sample_2.txt");
        List<Vote> votes = input.getVotes();
        logic logic = new logic(votes);

        List<Party> parties = logic.createParties();
        List<Riding> ridings = logic.createRidings();
        List<Candidate> candidates = logic.createCandidates(parties, ridings);

        Proportional proportional = new Proportional(votes, parties, ridings, candidates);
        proportional.countVotes();

        ArrayList<Integer> results = new ArrayList<Integer>();
        for (int i = 0; i < parties.size(); i++){
            results.add(parties.get(i).getSeats());
        }

        assertEquals(expected, results);
    }
    
    /**
     * Test winner of first past the post in Sample_2.txt
     * 
     * @throws IOException
     */
    @Test
    public void testSampleTwoFirstPast() throws IOException{
        String expected = "Craig Alvarado";
        
        //Use appropriate path here
        Input input = new Input("G:/CompSci_Sem_3/Programming_III/Project/programming_iii_project_2021/VoteInput/Sample_2.txt");
        List<Vote> votes = input.getVotes();
        logic logic = new logic(votes);

        List<Party> parties = logic.createParties();
        List<Riding> ridings = logic.createRidings();
        List<Candidate> candidates = logic.createCandidates(parties, ridings);

        First_Past firstPast = new First_Past(votes, parties, ridings, candidates);
        firstPast.countVotes();

        assertEquals(expected, firstPast.getWinner().getName());
    }
    



    /**
     * Sample_3.txt test cases
     */


    /**
     * Test number of votes in Sample_3.txt
     * 
     * @throws IOException
     */
    @Test
    public void testSampleThreeVotes() throws IOException{
        int expected = 587;
        
        //Use appropriate path here
        Input input = new Input("G:/CompSci_Sem_3/Programming_III/Project/programming_iii_project_2021/VoteInput/Sample_3.txt");
        List<Vote> votes = input.getVotes();

        assertEquals(expected, votes.size());
    }

    /**
     * Test number of parties in Sample_3.txt
     * 
     * @throws IOException
     */
    @Test
    public void testSampleThreeParties() throws IOException{
        int expected = 5; //Andrew sets his own filepath
        
        //Use appropriate path here
        Input input = new Input("G:/CompSci_Sem_3/Programming_III/Project/programming_iii_project_2021/VoteInput/Sample_3.txt");
        List<Vote> votes = input.getVotes();
        logic logic = new logic(votes);

        List<Party> parties = logic.createParties();
        assertEquals(expected, parties.size());
    }

    /**
     * Test number of ridings in Sample_3.txt
     * 
     * @throws IOException
     */
    @Test
    public void testSampleThreeRidings() throws IOException{
        int expected = 5; //Andrew sets his own filepath
        
        //Use appropriate path here
        Input input = new Input("G:/CompSci_Sem_3/Programming_III/Project/programming_iii_project_2021/VoteInput/Sample_3.txt");
        List<Vote> votes = input.getVotes();
        logic logic = new logic(votes);

        List<Riding> ridings = logic.createRidings();
        assertEquals(expected, ridings.size());
    }

    /**
     * Test number of candidates in Sample_3.txt
     * 
     * @throws IOException
     */
    @Test
    public void testSampleThreeCandidates() throws IOException{
        int expected = 25; //Andrew set his own filepath
        
        //Use appropriate path here
        Input input = new Input("G:/CompSci_Sem_3/Programming_III/Project/programming_iii_project_2021/VoteInput/Sample_3.txt");
        List<Vote> votes = input.getVotes();
        logic logic = new logic(votes);

        List<Party> parties = logic.createParties();
        List<Riding> ridings = logic.createRidings();
        List<Candidate> candidates = logic.createCandidates(parties, ridings);

        assertEquals(expected, candidates.size());
    }

    /**
     * Test winner of majority vote in Sample_3.txt
     * 
     * @throws IOException
     */
    @Test
    public void testSampleThreeMajority() throws IOException{
        String expected = "Java"; //Andrew set his own filepath
        
        //Use appropriate path here
        Input input = new Input("G:/CompSci_Sem_3/Programming_III/Project/programming_iii_project_2021/VoteInput/Sample_3.txt");
        List<Vote> votes = input.getVotes();
        logic logic = new logic(votes);

        List<Party> parties = logic.createParties();
        List<Riding> ridings = logic.createRidings();
        List<Candidate> candidates = logic.createCandidates(parties, ridings);

        Majority majority = new Majority(votes, parties, ridings, candidates);
        majority.countVotes();

        assertEquals(expected, majority.getWinner().getPartyName());
    }

    /**
     * Test winner of proportional vote in Sample_3.txt
     * 
     * @throws IOException
     */
    @Test
    public void testSampleThreeProportional() throws IOException{
        ArrayList<Integer> expected = new ArrayList<Integer>();
        
        expected.add(0);
        expected.add(5);
        expected.add(0);
        expected.add(0);
        expected.add(0);
        
        //Use appropriate path here
        Input input = new Input("G:/CompSci_Sem_3/Programming_III/Project/programming_iii_project_2021/VoteInput/Sample_3.txt");
        List<Vote> votes = input.getVotes();
        logic logic = new logic(votes);

        List<Party> parties = logic.createParties();
        List<Riding> ridings = logic.createRidings();
        List<Candidate> candidates = logic.createCandidates(parties, ridings);

        Proportional proportional = new Proportional(votes, parties, ridings, candidates);
        proportional.countVotes();

        ArrayList<Integer> results = new ArrayList<Integer>();
        for (int i = 0; i < parties.size(); i++){
            results.add(parties.get(i).getSeats());
        }

        assertEquals(expected, results);
    }

    /**
     * Test winner of first past the post in Sample_3.txt
     * 
     * @throws IOException
     */
    @Test
    public void testSampleThreeFirstPast() throws IOException{
        String expected = "Lucius McCarty";
        
        //Use appropriate path here
        Input input = new Input("G:/CompSci_Sem_3/Programming_III/Project/programming_iii_project_2021/VoteInput/Sample_3.txt");
        List<Vote> votes = input.getVotes();
        logic logic = new logic(votes);

        List<Party> parties = logic.createParties();
        List<Riding> ridings = logic.createRidings();
        List<Candidate> candidates = logic.createCandidates(parties, ridings);

        First_Past firstPast = new First_Past(votes, parties, ridings, candidates);
        firstPast.countVotes();

        assertEquals(expected, firstPast.getWinner().getName());
    }
}