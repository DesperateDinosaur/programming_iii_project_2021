import java.io.IOException;
import java.util.List;

public class VotingApp {
    //fields
    private Input input;
    private logic logic;
    private List<Vote> votes;
    private List<Party> parties;
    private List<Riding> ridings;
    private List<Candidate> candidates;
    private Boolean voteConducted;

    /**
     * this constructor sets up all the required fields using the inputed path
     * @param path
     * @throws IOException
     */
    public VotingApp(String path) throws IOException{
        this.input = new Input(path);
        this.votes = this.input.getVotes();
        this.logic = new logic(this.votes);
        this.parties = this.logic.createParties();
        this.ridings = this.logic.createRidings();
        this.candidates = this.logic.createCandidates(this.parties, this.ridings);
    }

    /**
     * this method takes a string as input and checks if it matches 1 of 3 constants,
     * if it does it will create an extended type of Poll, count the votes and return the extended Poll object
     * @param choice
     * @return Poll
     */
    public Poll doElection(String choice){
        final String MAJORITY = "Majority Vote";
        final String PROPORTIONAL = "Proportional Representation";
        final String FIRSTPAST = "First Past The Post";
        Poll election = null;
            if(choice.equals(MAJORITY)){
                election = new Majority(this.votes, this.parties, this.ridings, this.candidates);
            }
            else if(choice.equals(PROPORTIONAL)){
                election = new Proportional(this.votes, this.parties, this.ridings, this.candidates);
            }
            else if(choice.equals(FIRSTPAST)){
                election = new First_Past(this.votes, this.parties, this.ridings, this.candidates);
            }
        this.voteConducted = true;
        election.countVotes();
        return election;
    }

    /**
     * this method returns voteConducted
     * @return boolean
     */
    public Boolean getVoteConducted(){
        return this.voteConducted;
    }

    /**
     * this method returns the input object
     * @return Input
     */
    public Input getInput(){
        return this.input;
    }
}