public class Party{
    //fields
    private String partyName;
    private int numVotes;
    private int numSeats;

    /**
     * This constructor takes a name String as input and sets the partyName fields to this String
     * @param name
     */
    public Party(String name){
        this.partyName = name;
    }
    
    /**
     * Returns number of seats
     * @return
     */
    public int getSeats() {
        return this.numSeats;
    }

    /**
     * Sets number of seats
     * @param seats
     */
    public void setSeats(int seats){
        this.numSeats = seats;
    }

    /**
     * Returns number of votes
     * @return
     */
    public int getNumVotes(){
        return this.numVotes;
    }

    /**
     * Returns party name
     * @return
     */
    public String getPartyName(){
        return this.partyName;
    }

    /**
     * Increments vote counter by 1
     */
    public void addVote(){
        this.numVotes = this.numVotes + 1;
    }

    /**
     * Returns string with party name and how many votes they have
     */
    public String toString(){
        return this.partyName + ": " + this.numVotes + "votes";
    }
}