public class Candidate {
    private String name;
    private Party party;
    private Riding riding;
    private int numVotes;

    /**
     * This constructor takes a name String as input and sets the name field to this String
     * numVotes is initialized to 0
     */
    public Candidate(String name){
        this.name = name;
        this.numVotes = 0;
    }

    /**
     * Returns the candidates name
     * @return String
     */
    public String getName() {
        return this.name;
    }

    /**
     * Returns party that the candidate belongs to
     * @return Party
     */
    public Party getParty() {
        return this.party;
    }

    /**
     * Returns riding that the candidate belongs to
     * @return Riding
     */
    public Riding getRiding() {
        return this.riding;
    }

    /**
     * Sets party that the candidate belongs to
     * @param Party
     */
    public void setParty(Party p){
        this.party = p;
    }

    /**
     * Sets riding that the candidate belongs to
     * @param r
     */
    public void setRiding(Riding r){
        this.riding = r;
    }
    
    /**
     * Increments the candidates vote count by 1
     */
    public void addVote(){
        this.numVotes = this.numVotes + 1;
    }

    /**
     * Returns the number of votes the candidate has received
     * @return int
     */
    public int getNumVotes(){
        return this.numVotes;
    }

    /**
     * This function returns a String describing the candidate's name and which party and riding they belong to
     */
    public String toString(){
        return this.name + ", " + this.party.getPartyName() + " party, " + this.riding.getRidingName(); 
    }
}