import java.util.ArrayList;
import java.util.List;

public class logic {
    //fields
    private List<Vote> votes;

    /**
     * This constructor takes a Vote List as input and sets the votes field to this List
     * @param input
     */
    public logic(List<Vote> input){
        this.votes = input;
    }

    /**
     * Parses through the Vote List field and sorts by parties
     * Duplicate parties are dropped to create a Party object List with no duplicates
     * @return List<Party>
     */
    public List<Party> createParties(){
        /*Generate two blank ArrayLists
          The StringList stores a list of parties with no duplicates
          The Party List stores a list of Party objects created using the reduced party list*/
        List<String> reduced = new ArrayList<>();
        List<Party> parties = new ArrayList<>();

        //Loop through the Vote List and Fill reduced StringList with party names while removing duplicates
        for (int i = 0; i < this.votes.size(); i++){
            if (!reduced.contains(this.votes.get(i).getParty())){
                reduced.add(this.votes.get(i).getParty());
            }
        }

        //Loop through the reduced StringList, create Party objects for each party and add each object to the Party List
        for (int i = 0; i < reduced.size(); i++){
            Party p = new Party(reduced.get(i));
            parties.add(p);
        }
        return parties;
    }

    /**
     * Parses through the Vote List field and sorts by ridings
     * Duplicate ridings are dropped to create a Riding object List with no duplicates
     * @return List<Riding>
     */
    public List<Riding> createRidings(){
        /*Generate two blank ArrayLists
          The StringList stores a list of ridings with no duplicates
          The Party List stores a list of Riding objects created using the reduced riding list*/
        List<String> reduced = new ArrayList<>(); 
        List<Riding> ridings = new ArrayList<>();

        //Loop through the Vote List and Fill reduced StringList with riding names while removing duplicates
        for (int i = 0; i < this.votes.size(); i++){
            if (!reduced.contains(this.votes.get(i).getRiding())){
                reduced.add(this.votes.get(i).getRiding());
            }
        }

        //Loop through the reduced StringList, create Riding objects for each riding and add each object to the Riding List
        for (int i = 0; i < reduced.size(); i++){
            Riding r = new Riding(reduced.get(i));
            ridings.add(r);
        }
        return ridings;
    }

    /**
     * Takes Party List and Riding List as input
     * Parses through the Vote List and sorts by candidates
     * Duplicate candidates are dropped to help with creating a Candidate object List with no duplicates
     * Vote List and Riding List are parsed through during the creation of candidate objects,
     * since candidates have Party and Riding objects as fields
     * @param parties
     * @param ridings
     * @return List<Candidate>
     */
    public List<Candidate> createCandidates(List<Party> parties, List<Riding> ridings){
        /*Generate two blank ArrayLists
          The StringList stores a list of candidates with no duplicates
          The Candidate List stores a list of Candidate objects created using the reduced candidate list*/
        List<String> reduced = new ArrayList<>();
        List<Candidate> candidates = new ArrayList<>();

        //Loop through the Vote List and Fill reduced StringList with candidate names while removing duplicates
        for (int i = 0; i < this.votes.size(); i++){
            if (!reduced.contains(this.votes.get(i).getCandidate())){
                reduced.add(this.votes.get(i).getCandidate());
            }
        }

        //Loop through the reduced StringList, create Candidate objects for each candidate and add each object to the Candidate List
        for (int i = 0; i < reduced.size(); i++){
            Candidate c = new Candidate(reduced.get(i));
            candidates.add(c);
        }

        /*Loop through the Vote List, Candidate List, and Party List
          Assign the Party field of each candidate to the object of the Party they belong to*/
        for(int i = 0; i < this.votes.size(); i++){
            for(int j = 0; j < candidates.size(); j++){
                if(this.votes.get(i).getCandidate().equals(candidates.get(j).getName())){
                    for(int k = 0; k < parties.size(); k++){
                        if(this.votes.get(i).getParty().equals(parties.get(k).getPartyName())){
                            candidates.get(j).setParty(parties.get(k));
                        }
                    }
                }
            }
        }

        /*Loop through the Vote List, Candidate List, and Riding List
          Assign the Riding field of each candidate to the object of the Riding they belong to*/
        for(int i = 0; i < this.votes.size(); i++){
            for(int j = 0; j < candidates.size(); j++){
                if(this.votes.get(i).getCandidate().equals(candidates.get(j).getName())){
                    for(int k = 0; k < ridings.size(); k++){
                        if(this.votes.get(i).getRiding().equals(ridings.get(k).getRidingName())){
                            candidates.get(j).setRiding(ridings.get(k));
                        }
                    }
                }
            }
        }
        return candidates;
    }
}