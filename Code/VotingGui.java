import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.stage.*;
import javafx.util.Duration;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;

public class VotingGui extends Application {
    //fields
    private VotingApp app;
    private String path;
    
    //Main GUI
    public void start(Stage stage) throws FileNotFoundException {

        //making scene and root
        Group root = new Group(); 
        Scene scene = new Scene(root, 700, 1000); 
        scene.setFill(Color.GRAY);
        
        //associate scene to stage and show
        stage.setTitle("Voting Systems Inc."); 
        stage.setScene(scene); 
    
        //making hbox and vbox
        BorderPane bp = new BorderPane();
        VBox vbox1 = new VBox();
        VBox intro = new VBox();
        HBox buttons = new HBox();
        HBox resultTxt = new HBox();
        HBox saveBox = new HBox();

        //making filechooser
        FileChooser fileChoice = new FileChooser();
        String defaultPath = Paths.get(".").toAbsolutePath().normalize().toString() + "/VoteInput";
        fileChoice.getExtensionFilters().add(new FileChooser.ExtensionFilter("Text Files", "*.txt"));
        fileChoice.setInitialDirectory(new File(defaultPath));

        //making images for buttons
        FileInputStream input1 = new FileInputStream("images/folder-open-solid.png");
        Image img1 = new Image(input1);
        ImageView loadImg = new ImageView(img1);
        loadImg.setFitHeight(20);
        loadImg.setFitWidth(20);
        FileInputStream input2 = new FileInputStream("images/save-solid.png");
        Image img2 = new Image(input2);
        ImageView saveImg = new ImageView(img2);
        saveImg.setFitHeight(20);
        saveImg.setFitWidth(20);

        //making buttons
        Button b1 = new Button("Majority Vote");
        Button b2 = new Button("Proportional Representation");
        Button b3 = new Button("First Past The Post");
        Button fileBtn = new Button();
        Button saveBtn = new Button();

        //making text
        Text startMsg = new Text("Please select a file by clicking the folder"+"\n");
        Text choiceMsg = new Text("\n" + "Select one of the voting styles below" + "\n");
        Text resultMsg = new Text("\n" + "Results" + "\n");

        //styling Text
        startMsg.setFont(Font.font("Verdana", FontWeight.BOLD, 15));
        startMsg.setTextAlignment(TextAlignment.CENTER);
        choiceMsg.setFont(Font.font("Verdana", FontWeight.BOLD, 15));
        choiceMsg.setTextAlignment(TextAlignment.CENTER);
        resultMsg.setFont(Font.font("Verdana", FontWeight.BOLD, 15));
        resultMsg.setTextAlignment(TextAlignment.CENTER);

        //setting posistions
        vbox1.setAlignment(Pos.CENTER);
        intro.setAlignment(Pos.CENTER);
        buttons.setAlignment(Pos.CENTER);
        resultTxt.setAlignment(Pos.CENTER);
        saveBox.setAlignment(Pos.CENTER);
        root.setTranslateX(150);
        root.setTranslateY(50);

        //styling buttons
        b1.setStyle("-fx-background-color: #6bff69;");
        b2.setStyle("-fx-background-color: #ff6969;");
        b3.setStyle("-fx-background-color: #f3ff69;");
        fileBtn.setGraphic(loadImg);
        saveBtn.setGraphic(saveImg);
        buttons.setSpacing(10);

        //styling and tooltips
        Tooltip tt = new Tooltip("Save current results to /Output/GuiResultsLog.txt");
        tt.setStyle("-fx-font: normal bold 15 langdon; -fx-base: #AE3522; -fx-text-fill: orange;");
        tt.setShowDelay(Duration.seconds(0.25));
        saveBtn.setTooltip(tt);

        Tooltip tt2 = new Tooltip("Counts which party recieved the most votes");
        tt2.setStyle("-fx-font: normal bold 15 langdon; -fx-base: #AE3522; -fx-text-fill: orange;");
        tt2.setShowDelay(Duration.seconds(0.25));
        b1.setTooltip(tt2);

        Tooltip tt3 = new Tooltip("Counts how many ridings were won by each party");
        tt3.setStyle("-fx-font: normal bold 15 langdon; -fx-base: #AE3522; -fx-text-fill: orange;");
        tt3.setShowDelay(Duration.seconds(0.25));
        b2.setTooltip(tt3);

        Tooltip tt4 = new Tooltip("Counts which candidate recieved enough votes to pass the post or the most votes");
        tt4.setStyle("-fx-font: normal bold 15 langdon; -fx-base: #AE3522; -fx-text-fill: orange;");
        tt4.setShowDelay(Duration.seconds(0.25));
        b3.setTooltip(tt4);

        //adding buttons and text to the root
        intro.getChildren().addAll(startMsg, fileBtn);
        buttons.getChildren().addAll(b1,b2,b3);
        resultTxt.getChildren().addAll(resultMsg);
        vbox1.getChildren().addAll(choiceMsg, buttons, resultTxt);
        saveBox.getChildren().addAll(saveBtn);
        bp.setTop(intro);
        bp.setCenter(vbox1);
        bp.setBottom(saveBox);
        root.getChildren().add(bp);

        //adding events to buttons
        b1.setOnAction(e ->{
            handleVotingChoice(e, b1.getText(), resultMsg);
        });
        b2.setOnAction(e ->{
            handleVotingChoice(e, b2.getText(), resultMsg);
        });
        b3.setOnAction(e ->{
            handleVotingChoice(e, b3.getText(), resultMsg);
        });
        fileBtn.setOnAction(e -> {
            File choice = fileChoice.showOpenDialog(stage);
            if(choice != null){
                this.path = choice.getAbsolutePath();
            }
        });
        saveBtn.setOnAction(e -> {
            if(this.app == null){
                resultMsg.setText("\n"+"Please select a file to load before saving"+"\n");
            }
            else if(this.app == null || this.app.getVoteConducted() != true){
                resultMsg.setText("\n"+"Please select a file to load before saving"+"\n");
            }
            else{
                Output output = new Output(Paths.get(".").toAbsolutePath().normalize().toString()+"/Output/GuiResultsLog.txt");
                output.createFile(resultMsg);
            }
        });
        
        //display stage       
        stage.show(); 
        stage.centerOnScreen();
    }
    
    /**
     * this method checks if the path variable was set then creates a votingApp object to run the election,
     * the results of the election are then set to resultText
     * @param e
     * @param buttonText
     * @param resultText
     */
    public void handleVotingChoice(ActionEvent e, String buttonText, Text resultText){
        try{
            if(this.path == null){resultText.setText("\n" + "Please select a file before counting Votes" + "\n");}
            else{
                this.app = new VotingApp(this.path);
                if(this.app.getInput().getFileReadSuccess() == true){
                    Poll result = app.doElection(buttonText);
                    resultText.setText(result.toString());
                }
                else{
                    resultText.setText("\n"+"Please select a valid text file"+"\n");
                }
            }
        }
        catch(IOException err){
            System.out.println("Error during " + buttonText + "event " + err);
        } 
    }
        
    public static void main(String[] args) {
        launch();
    }
}