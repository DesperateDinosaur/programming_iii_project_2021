public abstract class FileIO{
    //fields
    protected String filepath;

    /**
     * This constructor takes a filepath String as input and sets the filepath field to this String
     * @param filepath
     */
    public FileIO(String filepath){
        this.filepath = filepath;
    }

    /**
     * Returns the filepath field
     * @return String
     */
    public String getFilepath(){
        return this.filepath;
    }
}