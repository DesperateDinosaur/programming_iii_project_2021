public class Vote {
    private String name;
    private String party;
    private String riding; 
    
    /**
     * This constructor takes a name String, party name String, 
     * and riding name String as input and sets the relevant fields to these inputs
     * @param name
     * @param p
     * @param r
     */
    public Vote(String name, String p, String r){
        this.name = name;
        this.party = p;
        this.riding = r;
    }
    
    /**
     * Returns the candidate name written on the ballot
     * @return
     */
    public String getCandidate() {
        return name;
    }

    /**
     * Returns the party name written on the ballot
     * @return
     */
    public String getParty() {
        return party;
    }

    /**
     * Returns the ridinig name written on the ballot
     * @return
     */
    public String getRiding() {
        return riding;
    }

    /**
     * Take a vote as input and check if it exists
     * @param vote
     * @return boolean
     */
    public boolean Contains(Vote vote){
        if(this.name == vote.getCandidate() && this.party == vote.getParty() && this.riding == vote.getRiding()){
            return true;
        }
        return false;
    }
}