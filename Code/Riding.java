public class Riding {
    //fields
    private String ridingName;
    private String leader;

    /**
     * This constructor takes a String name as input and sets ridingName
     * @param name
     */
    public Riding(String name){
        this.ridingName = name;
    }

    /**
     * Returns the name of the riding
     * @return String
     */
    public String getRidingName(){
        return this.ridingName;
    }

    /**
     * Returns the name of the riding leader
     * @return String
     */
    public String getLeader(){
        return this.leader;
    }

    /**
     * Sets the leader name of the riding with String input
     * @param leader
     */
    public void setLeader(String leader){
        this.leader = leader;
    }

    /**
     * Returns String describing the name of the riding along with the name of it's leader
     * @return String
     */
    public String toString(){
        return "The Riding: " + this.ridingName + " was won by " + this.leader;
    }

}