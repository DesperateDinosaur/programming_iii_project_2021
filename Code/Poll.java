import java.util.List;

public abstract class Poll {
    //fields
    protected List<Vote> votes;
    protected List<Party> parties;
    protected List<Riding> ridings;
    protected List<Candidate> candidates;

    /**
     * This constructor takes a vote List, party List, riding List, 
     * and candidate List as input and sets the relevant fields to these inputs
     * @param votes
     * @param parties
     * @param ridings
     * @param candidates
     */
    public Poll(List<Vote> votes, List<Party> parties, List<Riding> ridings, List<Candidate> candidates){
        this.votes = votes;
        this.parties = parties;
        this.ridings = ridings;
        this.candidates = candidates;
    }

    /**
     * Pre-defined method header that sub-classes must implement
     */
    public abstract void countVotes();
}