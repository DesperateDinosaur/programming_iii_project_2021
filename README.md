## Name
Voting System Manager

## Description
This project is a object oriented program that allows users to load files from their machine and count votes from it.

The votes can be counted using different styles and the results can be saved to an external file/

## Installation
-Make sure you have a connection to the dawson shares drive (S:/)
-Make sure the vmargs are set up correctly in launch.json
-Make sure you set the abolute path to the input file in JUnit

## Usage
-Launch the main in VotingGui.java
-Click the folder button
-Select a valid text file from your machine (name, party, riding)
-Click one of the voting buttons (Majority, Proportion, First Past)
-View the results and the breakdown
-Click the save icon to append to /Output/GuiResultsLog.txt


## Support
-Final UML design is located in /phases/phase3/finalUML.png
-Previous design docs and sumissions can be found in /phases/
-Hover over the buttons for a description of what they do

## Roadmap
-More voting Styles
-Fix Exception when User clicks save after being told thier file is invalid (might need to rework entire GUI)

## Contributing
-Lee Vecchiarelli
-Gabriel Internoscia

## Acknowledgment

Lee
``````````````````
-Bug fixing in logic class
-Finihsed Output class + bugs
-VotingGUI class
-GUI input validation, event handlers, bugs, styling
-FileIO bugs
-Some Jdocs
-comments
-Party class
-Candidate class
-Riding class
-Vote class
-Majority class
-Most of Input class
-Clean up
-Most of final UML

Gabriel
``````````````````
-Set up class skeleton
-printing errors
-JUnit tests
-Candidate class
-Most of Riding class
-Most of Logic class
-Most of Party class
-bug fixing in logic class
-Started Output class
-FileIO class
-Vote class
-GUI bugs
-Proportional class
-Most of Candidate class
-Most of Jdocs
-Comments
-Finihsed final UML

## License
Private project required for a cegep grade

## Project status
Completed

